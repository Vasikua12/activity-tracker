import React, { Component } from 'react';

import axios from 'axios';
import './App.css';
import TrackerList from './containers/list';
import TrackerMax from './containers/max';
import AddActivity from './containers/add';

class App extends Component{
state = {
  trainingList: [],
  trainingMaxRide: [],
  trainingMaxRun: [],
  trainingTotalRide: '',
  trainingTotalRun: ''
}

componentDidMount() {
  axios.get(`http://localhost:5000/all`)
    .then(res => {
      const trainingList = res.data;
      this.setState({ trainingList });
    })
  axios.get(`http://localhost:5000/maxRide`)
  .then(res => {
    const trainingMaxRide = res.data;
    this.setState({ trainingMaxRide });
  })
  axios.get(`http://localhost:5000/maxRun`)
  .then(res => {
    const trainingMaxRun = res.data;
    this.setState({ trainingMaxRun });
  })
  axios.get(`http://localhost:5000/totalRide`)
  .then(res => {
    const trainingTotalRide = res.data[0].TotalRide.toFixed(2);
    this.setState({ trainingTotalRide });
  })
  axios.get(`http://localhost:5000/totalRun`)
  .then(res => {
    const trainingTotalRun = (res.data[0].TotalRun).toFixed(2);
    this.setState({ trainingTotalRun });
  })
}
  render() {
    console.log('HELLO ',this.state.trainingTotalRide);
  return (
    <div className="App">
      <header className="App-header">
        <div className="titel"><h1>Activity tracker</h1></div>
        <div className="add">
          <AddActivity />
        </div>
        <div className="tracker">
          <div className="contents">
            <TrackerList trainingLists={this.state.trainingList}/>
          </div>
          <div className="module">
            <div className="max">
              <TrackerMax trainingMaxRide={this.state.trainingMaxRide} trainingMaxRun={this.state.trainingMaxRun}/>
            </div>
            <div className="distance">
              <div className="title">
                <p><strong>Total ride distance: </strong></p>
                <p><strong>Total run distance: </strong></p>
              </div>
              <div className="total">
                <p>{this.state.trainingTotalRide}</p>
                <p>{this.state.trainingTotalRun}</p>
              </div>
            </div>
          </div>
        </div>
      </header>
    </div>
  );

}
}
export default App;
