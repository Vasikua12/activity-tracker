import React from 'react';
import axios from 'axios';

export default class AddActivity extends React.Component {
  state = {
    date: Date.now(),
    start: '',
    finish: '',
    typeActivity: '',
    distance: '',
    speed: '',
  }

  handleChange = event => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit = event => {
    event.preventDefault();

    let myAxios = axios.create({
      xsrfCookieName: 'csrftoken',
      xsrfHeaderName: 'X-CSRFTOKEN',
      headers: {
        "Content-Type": "application/json"
      }
    });

    console.log('Date ', this.state.date);
    console.log('Start ', this.state.start);
    console.log('Finish ', this.state.finish);
    console.log('Distance', this.state.distance);
    console.log('typeActivity', this.state.typeActivity);
    console.log('Speed', this.state.speed);
    let getDate = (string) => new Date(0, 0,0, string.split(':')[0], string.split(':')[1]);
    let timeIn = (getDate(this.state.finish) - getDate(this.state.start))/1000/60;

    const activity = {
      date: new Date(this.state.date),
      typeActivity: this.state.typeActivity,
      distance: this.state.distance,
      time: timeIn,
      speed: this.state.distance / (timeIn/60),
    };

    console.log('POST', activity);
    if (this.state.start !== '' && this.state.finish !== '' && this.state.distance !== '' && this.state.typeActivity !== '') {
    myAxios
    .post(`http://localhost:5000/add`,
      /*"date": new Date(this.state.date),
      "typeActivity": this.state.typeActivity,
      "distance": this.state.distance,
      "time": timeIn,
      "speed": this.state.distance / (timeIn/60),*/
      activity
      )
      .then(res => {
        console.log(res);
      })
      window.location.reload(false);
    } else {
      window.alert("Please write full information")
    }

  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label><strong>Add new activity:</strong></label>

        <input
          className="time"
          type="time"
          name="start"
          value={this.state.start}
          onChange={this.handleChange}
          placeholder="Start time"
        />
        <input
          className="time"
          type="time"
          name="finish"
          value={this.state.finish}
          onChange={this.handleChange}
          placeholder="Finish time"
        />
        <input
          type="text"
          name="distance"
          value={this.state.distance}
          onChange={this.handleChange}
          placeholder="Distance"
        />
        <select name="typeActivity" onChange={this.handleChange}>
          <option disabled selected>Select activity type</option>
          <option value="Ride">Ride</option>
          <option value="Run">Run</option>
        </select>
        <input className="submit" type="submit" value="Save"/>
      </form>
    )
  }
}
