import React from 'react';
import Table from 'react-bootstrap/Table'

var month = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");


const Trackerlist = ({trainingLists}) => {
    console.log('test',trainingLists)
    return (
        <div>
        { trainingLists.map((trainingLists, index) =>
            (
                <div className="all">
                <Table responsive>
                    <tbody>
                        <tr>
                            <td className="allDate" key={index}>{ month[new Date(trainingLists.date).getMonth()] } {new Date(trainingLists.date).getDate()}</td>
                            <td >{trainingLists.typeActivity}</td>
                            <td>{trainingLists.distance + " km"}</td>
                            <td>{trainingLists.time > 60 ? Math.trunc(trainingLists.time / 60) + ' h ' + trainingLists.time % 60 + ' m': trainingLists.time + ' m'}</td>
                            <td>{trainingLists.speed + ' km/h'}</td>
                        </tr>
                    </tbody>
                </Table>
                </div>

            )
        )}


      </div>
    )

};
export default Trackerlist;