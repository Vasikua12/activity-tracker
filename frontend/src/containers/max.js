import React from 'react';

var month = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct", "Nov", "Decr");

const TrackerMax = ({trainingMaxRun, trainingMaxRide, index}) => {

  console.log('Ride', trainingMaxRide);
  console.log('Run',trainingMaxRun);

  return (
      <div>
        <h4>Longesy ride:</h4>
         { trainingMaxRide.map((trainingMaxRide, index) =>
            (
                <table key={index}>
                    <tbody>
                        <tr className="max">
                            <td >{ month[new Date(trainingMaxRide.date).getMonth()] } {new Date(trainingMaxRide.date).getDate()}</td>
                            <td >{trainingMaxRide.distance + " km"}</td>
                            <td >{trainingMaxRide.time > 60 ? Math.trunc(trainingMaxRide.time / 60) + ' h ' + trainingMaxRide.time % 60 + ' m': trainingMaxRide.time + ' m'}</td>
                        </tr>
                    </tbody>
                </table>

            )
        )}
        <h4>Longest run:</h4>
        { trainingMaxRun.map((trainingMaxRun, index) =>
            (
                <table key={index}>
                    <tbody>
                        <tr className="max">
                            <td >{ month[new Date(trainingMaxRun.date).getMonth()] } {new Date(trainingMaxRun.date).getDate()}</td>
                            <td >{trainingMaxRun.distance + " km"}</td>
                            <td >{trainingMaxRun.time > 60 ? Math.trunc(trainingMaxRun.time / 60) + ' h ' + trainingMaxRun.time % 60 + ' m': trainingMaxRun.time + ' m'}</td>
                        </tr>
                    </tbody>
                </table>

            )
        )}
        <p> </p>
        <p></p>
      </div>
  )
}
export default TrackerMax;