const sql = require("./db.js");

// constructor
const Tracker = function(tracker) {
  this.date = tracker.date;
  this.typeActivity = tracker.typeActivity;
  this.distance = tracker.distance;
  this.time = tracker.time;
  this.speed = tracker.speed;
};

Tracker.create = (newTracker, result) => {
  console.log('newTracker   ', newTracker)
  sql.query("INSERT INTO activ SET ?", newTracker, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created Tracker: ", { id: res.insertId, ...newTracker });
    result(null, { id: res.insertId, ...newTracker });
  });
};

Tracker.getAll = result => {
  sql.query("SELECT * FROM activ", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Tracker: ", res);
    result(null, res);
  });
};

Tracker.maxRide = result => {
    sql.query("SELECT `date`, `distance`, `time` FROM activ WHERE `distance` = (SELECT MAX(`distance`) FROM activ) AND `typeActivity` = 'Ride';", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("Tracker: ", res);
      result(null, res);
    });
  };

  Tracker.maxRun = result => {
    sql.query("SELECT `date`, `distance`, `time` FROM activ WHERE `distance` = (SELECT MAX(`distance`) FROM activ WHERE `typeActivity` = 'Run');", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("Tracker: ", res);
      result(null, res);
    });
  };
  Tracker.totalRide = result => {
    sql.query("select sum(if(`typeActivity` = 'Ride',`distance`,0)) as TotalRide from activ;", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("Tracker: ", res);
      result(null, res);
    });
  };

  Tracker.totalRun = result => {
    sql.query("select sum(if(`typeActivity` = 'Run',`distance`,0)) as TotalRun from activ;", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("Tracker: ", res);
      result(null, res);
    });
  };

module.exports = Tracker;