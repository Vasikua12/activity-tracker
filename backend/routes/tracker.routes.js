var cors = require('cors');

module.exports = app => {
    const trackers = require("../controllers/tracker.controller.js");
  
    // Create a new Customer
    app.use("*", cors());
    app.options('*', cors())
    app.post("/add", cors(), trackers.create);
  
    // Retrieve all Customers
    app.get("/all", cors(), trackers.findAll);
  
    // Retrieve a single Customer with customerId
    app.get("/maxRide", cors(), trackers.findMaxRide);
    app.get("/maxRun", cors(), trackers.findMaxRun);
    app.get("/totalRide", cors(), trackers.findTotalRide);
    app.get("/totalRun", cors(), trackers.findTotalRun);
  
  };