-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.4.11-MariaDB - mariadb.org binary distribution
-- Операционная система:         Win64
-- HeidiSQL Версия:              10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных trackerdb
CREATE DATABASE IF NOT EXISTS `trackerdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `trackerdb`;

-- Дамп структуры для таблица trackerdb.activ
CREATE TABLE IF NOT EXISTS `activ` (
  `id_active` smallint(10) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `typeActivity` char(50) DEFAULT '0',
  `distance` float DEFAULT 0,
  `time` float DEFAULT 0,
  `speed` float DEFAULT 0,
  PRIMARY KEY (`id_active`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Дамп данных таблицы trackerdb.activ: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `activ` DISABLE KEYS */;
INSERT INTO `activ` (`id_active`, `date`, `typeActivity`, `distance`, `time`, `speed`) VALUES
	(1, '2019-09-22', 'Ride', 12.2, 28, 21),
	(2, '2019-09-25', 'Run', 3.4, 35, 6.4),
	(3, '2019-09-30', 'Run', 5, 30, 6),
	(4, '2019-10-15', 'Run', 3.4, 35, 6.4),
	(5, '2019-10-23', 'Ride', 25, 105, 16.2),
	(6, '2019-12-11', 'Run', 3.4, 35, 6.4),
	(11, '2020-01-01', 'Run', 2, 20, 6),
	(12, '2020-01-20', 'Ride', 2, 60, 2);
/*!40000 ALTER TABLE `activ` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
